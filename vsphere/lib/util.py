#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import subprocess


def file_exists(file_name):
    if os.path.islink(file_name):
        return True
    try:
        return bool(os.stat(file_name))
    except:
        return False


def run_cmd(cmd):
    p = subprocess.Popen(cmd, shell=True,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT)
    while True:
        next_line = p.stdout.readline()
        if next_line == '' and p.poll() is not None:
            break
        sys.stdout.write(next_line)
    p.stdout.close()
    p.wait()

    return p.returncode
