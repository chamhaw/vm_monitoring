from django.conf.urls import url
from . import views

urlpatterns = [
        url(r'^$', views.index, name='vm_index'),
        url(r'delete$', views.delete, name='vm_delete'),
        url(r'detail/(?P<uuid>[0-9a-zA-Z-]+)/$', views.vm_detail, name='vm_detail'),
        url(r'detail/(?P<uuid>[0-9a-zA-Z-]+)/rmdisk$', views.remove_disk, name='rmdisk'),
        url(r'detail/(?P<uuid>[0-9a-zA-Z-]+)/adddisk$', views.add_disk, name='adddisk'),
        url(r'deploy', views.deploy, name='vm_deploy'),
        url(r'poweroff$', views.poweroff, name='vm_poweroff'),
        url(r'poweron$', views.poweron, name='vm_poweron'),
]
