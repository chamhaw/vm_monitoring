#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Result entities """
from django.http import JsonResponse


class HttpJsonResult(JsonResponse):
    ''' Http result entity '''
    def __init__(self, ret_code, message="", data=None, **kwargs):
        self.ret_code = ret_code
        self.message = message
        self.data = data
        self.addtion = kwargs
        super(HttpJsonResult, self).__init__(self.todict())

    def todict(self):
        """ format to dict """
        return self.__dict__
